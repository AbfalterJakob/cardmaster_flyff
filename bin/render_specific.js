/**
 * Created by Jakob on 04.02.2017.
 */

const datapath = "resources/data.json";
const fs = require("fs");
const readline = require("readline");
const valid_tiers = ["DB", "BA", "AS", "SR"];

console.info("Hello, reading data from " + datapath);
let data = fs.readFileSync(datapath);
let jsondata = JSON.parse(data);
let moment = require('moment');
let tier, name;


console.info("Successfully read data");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter the tier of the card tradeup (2 letters) ', (answer) => {
    if( valid_tiers.indexOf(answer) === -1 ) {
        console.error("Not a valid tier");
        process.exit();
    }
    tier = answer;
    askForName();
});

function askForName() {
    rl.question('Please enter the name of contributer ', (answer) => {
        name = answer;
        calculateProbabilities();
    });
}

function calculateProbabilities() {
    let selector = tier + "_TIER";
    let tier_data = jsondata[selector];
    let success = 0;
    let failure = 0;
    tier_data.forEach(function (entry) {
            if( entry.name === name ) {
                success += entry.success;
                failure += entry.failure;
            }
        }
    );
    let total = success + failure;

    let probability = ( 100 / total ) * success;
    probability = probability.toFixed(2);
    console.log(name);
    console.log("Success: " + success);
    console.log("Failure: " + failure);
    console.log("Probability: " + probability);
    process.exit();
}
