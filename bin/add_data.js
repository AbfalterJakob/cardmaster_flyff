/**
 * Created by AbfalterJakob on 19.12.2016.
 */

const datapath = "resources/data.json";
const fs = require("fs");
const readline = require("readline");
const valid_tiers = ["DB", "BA", "AS", "SR"];
let tier, name, success, failure, total, date;

console.info("Hello, reading data from " + datapath);
let data = fs.readFileSync(datapath);
let jsondata = JSON.parse(data);
let moment = require('moment');

console.info("Successfully read data");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Please enter the tier of the card tradeup (2 letters) ', (answer) => {
    if( valid_tiers.indexOf(answer) === -1 ) {
        console.error("Not a valid tier");
        process.exit();
    }
    tier = answer;
    askForName();
});

function askForName() {
    rl.question('Please enter the name of contributer ', (answer) => {
        name = answer;
        askForSuccess();
    });
}

function askForSuccess() {
    rl.question('Please enter the amount of successes ', (answer) => {
        success = answer;
        askForFailure();
    });
}

function askForFailure() {
    rl.question('Please enter the amount of failures ', (answer) => {
        failure = answer;
        total = parseInt(failure) + parseInt(success);
        asForDate();
    });
}

function asForDate() {
    rl.question('Please enter the date of the entry (YYYY-MM-DD) or leave blank if today ', (answer) => {
        if( answer === '' ) {
            date = moment().format('YYYY-MM-DD');
        }
        else {
            date = answer;
        }
        saveData();
    });
}

function saveData() {
    let contribution = {
        "name" : name,
        "success" : parseInt(success),
        "failure" : parseInt(failure),
        "total" : total,
        "date" : date
    };

    switch(tier) {
        case 'DB' :
            jsondata.DB_TIER.push(contribution);
            break;
        case 'BA' :
            jsondata.BA_TIER.push(contribution);
            break;
        case 'AS' :
            jsondata.AS_TIER.push(contribution);
            break;
        case 'SR' :
            jsondata.SR_TIER.push(contribution);
            break;
        default :
            console.error("Invalid tier");
            break;
    }

    fs.writeFileSync(datapath, JSON.stringify(jsondata));
    console.info("Successfully wrote new data file");
    process.exit();
}



