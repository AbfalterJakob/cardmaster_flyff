/**
 * Created by AbfalterJakob on 19.12.2016.
 */

const datapath = "resources/data.json";
const templatepath = "resources/template.txt";
const outputpath = "resources/output.txt";
const fs = require("fs");

const datafile = fs.readFileSync(datapath, 'utf-8');
const template = fs.readFileSync(templatepath, 'utf-8');
const data = JSON.parse(datafile);

let tier_data = {
    db : {
        success : 0,
        failure:  0,
        total : 0,
        success_rate: 0,
        times_one_succes : 0,
        contributor: []
    },
    ba : {
        success : 0,
        failure:  0,
        total : 0,
        success_rate: 0,
        times_one_succes : 0,
        contributor: []
    },
    as : {
        success : 0,
        failure:  0,
        total : 0,
        success_rate: 0,
        times_one_succes : 0,
        contributor: []
    },
    sr : {
        success : 0,
        failure:  0,
        total : 0,
        success_rate: 0,
        times_one_succes : 0,
        contributor: []
    }
};

let contributors = [];
let entry;

// Iterate through DB_TIER entries
let db_tier_data = data.DB_TIER;
let ba_tier_data = data.BA_TIER;
let as_tier_data = data.AS_TIER;
let sr_tier_data = data.SR_TIER;

for( let i = 0; i < db_tier_data.length; i++ ) {
    entry = db_tier_data[i];
    add_data(tier_data.db, entry);
}

for( let i = 0; i < ba_tier_data.length; i++ ) {
    entry = ba_tier_data[i];
    add_data(tier_data.ba, entry);
}

for( let i = 0; i < as_tier_data.length; i++ ) {
    entry = as_tier_data[i];
    add_data(tier_data.as, entry);
}

for( let i = 0; i < sr_tier_data.length; i++ ) {
    entry = sr_tier_data[i];
    add_data(tier_data.sr, entry);
}

console.info("Finished populating data");

// Render output
let output = template;

output = output.replace('[DB_TOTAL]', tier_data.db.total);
output = output.replace('[DB_SUCCESS]', tier_data.db.success);
output = output.replace('[DB_FAILURE]', tier_data.db.failure);
output = output.replace('[DB_PROB]', tier_data.db.success_rate);
output = output.replace('[DB_AMOUNT]', tier_data.db.times_one_succes);
output = output.replace('[DB_CONTRIB]', printcontributors(tier_data.db.contributor));

output = output.replace('[BA_TOTAL]', tier_data.ba.total);
output = output.replace('[BA_SUCCESS]', tier_data.ba.success);
output = output.replace('[BA_FAILURE]', tier_data.ba.failure);
output = output.replace('[BA_PROB]', tier_data.ba.success_rate);
output = output.replace('[BA_AMOUNT]', tier_data.ba.times_one_succes);
output = output.replace('[BA_CONTRIB]', printcontributors(tier_data.ba.contributor));

output = output.replace('[AS_TOTAL]', tier_data.as.total);
output = output.replace('[AS_SUCCESS]', tier_data.as.success);
output = output.replace('[AS_FAILURE]', tier_data.as.failure);
output = output.replace('[AS_PROB]', tier_data.as.success_rate);
output = output.replace('[AS_AMOUNT]', tier_data.as.times_one_succes);
output = output.replace('[AS_CONTRIB]', printcontributors(tier_data.as.contributor));

output = output.replace('[SR_TOTAL]', tier_data.sr.total);
output = output.replace('[SR_SUCCESS]', tier_data.sr.success);
output = output.replace('[SR_FAILURE]', tier_data.sr.failure);
output = output.replace('[SR_PROB]', tier_data.sr.success_rate);
output = output.replace('[SR_AMOUNT]', tier_data.sr.times_one_succes);
output = output.replace('[SR_CONTRIB]', printcontributors(tier_data.sr.contributor));

let top = contributors.sort(compare);
top = top.slice(0,10);

output = output.replace('[ALL_CONTRIB]', printcontributors(top));

console.info("Successfully rendered template");
fs.writeFileSync(outputpath, output);
console.info("Wrote file to " + outputpath);

function add_data(tierdata, entry) {
    tierdata.success += entry.success;
    tierdata.failure += entry.failure;
    tierdata.total += entry.total;
    tierdata.success_rate = (( 100 / tierdata.total ) * tierdata.success).toFixed(2);
    tierdata.times_one_succes = Math.ceil( 100 / tierdata.success_rate );
    addcontribution(tierdata.contributor, entry.name, entry.total);
    addcontribution(contributors, entry.name, entry.total);
}

function addcontribution( arr, name, amount ) {
    for( let i = 0; i < arr.length; i++ ) {
        if( arr[i].name === name ) {
            arr[i].amount += amount;
            return;
        }
    }
    arr.push({name: name, amount: amount});
}

function printcontributors( arr ) {
    let sorted = arr.sort(compare);
    let printString = "";
    for ( let i = 0; i < sorted.length; i++ ) {
        let entry = sorted[i];
        printString += entry.name + " (" + entry.amount + ")\r\n";
    }
    return printString;
}

function compare( contributiona, contributionb ) {
    return contributionb.amount - contributiona.amount;
}

